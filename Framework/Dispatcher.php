<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 2014/11/26
 * Time: 0:49
 */

class Dispatcher {
    //PHPを設置するディレクトリのパス
    private $dir;

    /**
     * ディレクトリのセッター
     * @param $path　ファイルを設置するディレクトリのパス
     */
    public function setDir($path){
        $this->dir = $path;
    }

    /**
     * Dispatcher
     */
    public function Dispatch(){
        $param = INIT_STR;

        //リクエスト文字列からGETの?を除外した文字列を取得する
        $param = mb_substr($_SERVER['REQUEST_URI'], 0, mb_strpos($_SERVER['REQUEST_URI'], "?"));
        //?が存在しなかった場合、リクエスト文字列を取得する
        if($param === ""){
            $param = $_SERVER['REQUEST_URI'];
        }

        //コントローラー・アクションを取得

        //初期化
        $params = array();
        $controllerName = INIT_STR;
        $actionName = INIT_STR;

        $params = explode("/",$param);

        //値が存在したらコントローラー・アクションにセット
        //TODO 配列の添え字を設定値で変更できるように？
        if(array_key_exists(3,$params) and $params[3] !== "" and !is_null($params[3])){
            $controllerName = $params[3];
        }else{
            $controllerName = "Index";
        }
        if(array_key_exists(4,$params) and $params[4] !== "" and !is_null($params[4])){
            $actionName = $params[4];
        }else{
            $actionName = "index";
        }

        //クラス名・メソッド名設定
        $controllerClass = $controllerName."Controller";
        $actionMethod = $actionName."Action";

        //controller読み込み
        require_once $this->dir."/Controllers/".$controllerClass.".php";
        $controllerInstance = new $controllerClass($this->dir,$controllerName,$actionName);

        //action実行
        $controllerInstance->$actionMethod();


    }
} 