<?php
/**
 * Created by PhpStorm.
 * User: beat
 * Date: 2014/11/26
 * Time: 20:32
 */

class BaseController {
    protected  $Smarty;
    protected  $controllerName;
    protected  $actionName;
    protected  $thisDir;

    /**
     * @param $i_controllerName コントローラ名称
     * @param $i_actionName     アクション名称
     */
    public function BaseController($i_thisDir,$i_controllerName,$i_actionName) {
        //インスタンスごとのSmarty設定
        $this->Smarty = new Smarty();
        $this->controllerName = $i_controllerName;
        $this->actionName = $i_actionName;
        $this->thisDir = $i_thisDir;

        /*Smartyディレクトリ設定 */
        $this->Smarty->template_dir = $this->thisDir . "/View/templates";
        $this->Smarty->compile_dir  = $this->thisDir . "/View/templates_c";

        //各コントローラーの初期化処理
        $this::init();
    }
} 