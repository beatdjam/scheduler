<?php
/**
 * フロントコントローラー
 */

//基準ディレクトリ設定
$baseDir = dirname( __FILE__ )."/..";

//フレームワーク用処理読み込み
require_once $baseDir . "/Framework/Dispatcher.php";

//ベースコントローラー読み込み
require_once($baseDir . "/Common/BaseController.php");
//エラーコントローラー読み込み
require_once ($baseDir . "/Common/ErrorController.php");
//Smarty読み込み
require_once($baseDir . "/Library/Smarty/Smarty.class.php");

require_once $baseDir . "/Common/Const.php";

//エラー処理設定
set_error_handler(function ($errNo, $errStr, $errFile, $errLine){
    //エラーコントローラ読み込み
    $controllerInstance = new ErrorController($errNo, $errStr, $errFile, $errLine);
});

//ディスパッチ処理
$dispatcher = new Dispatcher();
$dispatcher->setDir($baseDir);
$dispatcher->dispatch();